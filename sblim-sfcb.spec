Name: sblim-sfcb
Summary: Small Footprint CIM Broker (SFCB)
URL: http://sblim.sourceforge.net/wiki/index.php/Sfcb
Version: 1.4.9
Release: 17
License: EPL-1.0

Source0: http://downloads.sourceforge.net/sblim/%{name}-%{version}.tar.bz2
Source1: sfcb.service

Patch0: sblim-sfcb-1.3.16-maxMsgLen.patch
Patch1: sblim-sfcb-1.4.8-default-ecdh-curve-name.patch
Patch2: sblim-sfcb-1.4.9-fix-ftbfs.patch
Patch3: sblim-sfcb-1.4.9-fix-null-deref.patch
Patch4: sblim-sfcb-1.4.9-fix-null-content-type-crash.patch
Patch5: sblim-sfcb-1.4.9-sfcbrepos-schema-location.patch
Patch6: sblim-sfcb-1.4.9-fix-multiple-definition.patch
Patch7: sblim-sfcb-1.4.9-fix-undefined-symbol.patch

Provides: cim-server = 0
Requires: cim-schema sblim-sfcCommon
BuildRequires: libcurl-devel perl-generators zlib-devel openssl-devel
BuildRequires: pam-devel cim-schema bison flex sblim-cmpi-devel
BuildRequires: systemd sblim-sfcCommon-devel openslp-devel gcc
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%Description
SFCB is a CIM server for resource-constrained and embedded environments.
It is written in C and designed to be modular and lightweight.

%prep
%autosetup -T -b 0 -p1 -n %{name}-%{version}

%build
%configure --enable-debug --enable-uds --enable-ssl --enable-pam --enable-ipv6 \
    --enable-slp --enable-large_volume_support --enable-optimized-enumeration --enable-relax-mofsyntax \
    CFLAGS="$CFLAGS -D_GNU_SOURCE -fPIE -DPIE" LDFLAGS="$LDFLAGS -Wl,-z,now -pie"

make

%install
%make_install

install -d %{buildroot}%{_unitdir}
install -p -m644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service

mkdir -p %{buildroot}%{_mandir}/man1/

rm %{buildroot}%{_sysconfdir}/init.d/sfcb
%delete_la

echo "%defattr(-,root,root,-)" > _pkg_list

find %{buildroot}%{_datadir}/sfcb -type f | grep -v %{buildroot}%{_datadir}/sfcb/CIM >> _pkg_list
sed -i s?%{buildroot}??g _pkg_list > _pkg_list_2
echo "%config(noreplace) %{_sysconfdir}/sfcb/*" >> _pkg_list
echo "%config(noreplace) %{_sysconfdir}/pam.d/*" >> _pkg_list
echo "%doc %{_docdir}/*" >> _pkg_list
echo "%{_mandir}/man1/*" >> _pkg_list
echo "%{_unitdir}/%{name}.service" >> _pkg_list
echo "%{_localstatedir}/lib/sfcb" >> _pkg_list
echo "%{_bindir}/*" >> _pkg_list
echo "%{_sbindir}/*" >> _pkg_list
echo "%{_libdir}/sfcb/*.so.*" >> _pkg_list
echo "%{_libdir}/sfcb/*.so" >> _pkg_list

cat _pkg_list

%pre
%{_bindir}/getent group sfcb >/dev/null || %{_sbindir}/groupadd -r sfcb
%{_sbindir}/usermod -a -G sfcb root > /dev/null 2>&1 || :

%post
%{_datadir}/sfcb/genSslCert.sh %{_sysconfdir}/sfcb &>/dev/null || :
/sbin/ldconfig
%{_bindir}/sfcbrepos -f > /dev/null 2>1
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart %{name}.service
if [ $1 -eq 0 ]; then
        %{_sbindir}/groupdel sfcb > /dev/null 2>&1 || :;
fi;

%files -f _pkg_list

%changelog
* Fri Aug 27 2021 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.4.9-17
- fix undefined symbol

* Fri Jul 30 2021 yaokai13 <yaokai13@huawei.com> - 1.4.9-16
- Fix failure caused by GCC upgrade to 10

* Tue Mar 16 2021 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.4.9-15
- Fix default location where sfcbrepos is looking for schema files

* Fri Feb 14 2020 Senlin Xia <xiasenlin1@huawei.com> - 1.4.9-14
- Package init
